import os
os.environ['DJANGO_SETTINGS_MODULE'] = 'project1.settings'
import django
#django.conf.settings.configure(default_settings, **settings)
from django.conf import settings

#settings.configure(DEBUG=True)

django.setup()




from app1.models import  Page, Category
#from app1.models import Page



def populate():
    """
        1. create a list of dictionaries with all the pages
        2. A dictionary of dictionaries for categories is cretaed
    """
    python_pages = [
         {"title": "The Python Tutorial",
            "url": "https://docs.python.org/3/tutorial/"},
         {
         "title":"How to Think like a Computer Scientist",
         "url":"http://www.greenteapress.com/thinkpython/"
         },
         {
            "title":"Learn Python in 10 Minutes",
            "url":"http://www.korokithakis.net/tutorials/python/"}
        
            ]

    django_pages = [
            {"title": "Official Django Tutorial",
             "url": "https://docs.djangoproject.com/en/3.1/intro/tutorial01/"},
            {"title":"Django Rocks",
            "url":"http://www.djangorocks.com/"
             },
            {
             "title":"How to Tango with Django",
             "url":"http://www.tangowithdjango.com/" 
             }
             ]

    other_pages = [
            {"title":"Bottle",
            "url":"http://bottlepy.org/docs/dev/"},
            {"title":"Flask",
            "url":"http://flask.pocoo.org"} 
            ]

    cats = {"Python": {"pages": python_pages, },
            "Django": {"pages": django_pages},
            "Other Frameworks": {"pages": other_pages} }    


    for cat, cat_data in cats.items():
        c = add_cat(cat)
        for p in cat_data["pages"]:
            add_page(c, p["title"], p["url"])


    for c in Category.objects.all():
        for p in Page.objects.filter(category=c):
            print("- {0} - {1}".format(str(c), str(p)))


def add_page(cat, title, url, views=0):
    p = Page.objects.get_or_create(category=cat, title=title)[0]
    p.url=url
    p.views=views
    p.save()
    return p


def add_cat(name):
    c = Category.objects.get_or_create(name=name, views=128, likes=64)[0]
    c.save()
    return c



if __name__ == '__main__':
    print("Starting App1 population script...")
    #os.environ.setdefault('DJANGO_SETTINGS_MODULES',
     #    'project1.settings')
    #django.setup()

    populate()
    #main()


