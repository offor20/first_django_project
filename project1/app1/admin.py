from django.contrib import admin
from app1.models import  Page, Category, UserProfile


admin.site.register(Category)
class PageAdmin(admin.ModelAdmin):
    list_display = ('category', 'title', 'url')

admin.site.register(Page, PageAdmin)
admin.site.register(UserProfile)