from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('about/', views.about, name='about'),
    # path('register/', views.register, name= 'register'),
    path('add_category/', views.add_category, name='add_category'),
   # path('login/', views.user_login, name='login'),
    path('restricted/', views.restricted, name= 'restricted'),
   # path('logout/', views.user_logout, name='logout'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
