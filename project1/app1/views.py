from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from django.urls import reverse
from django.http import HttpResponse, HttpResponseRedirect
from app1.forms import CategoryForm, UserProfileForm, UserForm
from django.contrib.auth.decorators import login_required
from datetime import datetime



def index(request):
  def visitor_cookie_handler(request, response):
# Get the number of visits to the site.
# We use the COOKIES.get() function to obtain the visits cookie.
# If the cookie exists, the value returned is casted to an integer.
# If the cookie doesn't exist, then the default value of 1 is used.
    visits = int(request.COOKIES.get('visits', '1'))
    last_visit_cookie = request.COOKIES.get('last_visit', str(datetime.now()))
    last_visit_time = datetime.strptime(last_visit_cookie[:-7],
          '%Y-%m-%d %H:%M:%S')
    # If it's been more than a day since the last visit...
    if (datetime.now() - last_visit_time).days > 0:
      visits = visits + 1
      #update the last visit cookie now that we have updated the count
      response.set_cookie('last_visit', str(datetime.now()))
    else:
      visits = 1
      # set the last visit cookie
      response.set_cookie('last_visit', last_visit_cookie)
      # Update/set the visits cookie
    response.set_cookie('visits', visits)
 # request.session.set_test_cookie()

  context_dict = {"message": "Ernest just started implementing Django Web App Called Jeva"}

  print(request.method)
  print(request.user)

  return render(request, 'app1/index.html', context=context_dict)
    #return HttpResponse("Welcome to jeva web application using Django. <br> <a href = '/jeva/about' > Learn More </a> ")

def about(request):
  if request.session.test_cookie_worked():
    print("TEST COOKIE WORKED!")
    request.session.delete_test_cookie()
  #  return HttpResponse("Here, you will find more information about jeva app. <br> <a href = '/app1/' > Home </a>")
  context_dict = {"sm_message": "This message is from the about page"}



  return render(request, 'app1/about.html', context=context_dict)

def add_category(request):
  form = CategoryForm()

  if request.method == 'POST':
       
    form = CategoryForm(request.POST)

    if form.is_valid():
      form.save(commit= True)
      return index(request)
             
    
    else:
      print(form.errors)

  return render(request, 'app1/add_category.html', {'form': form})




# def register(request):
#   # check if user already exists
#   is_a_user = False

#   if request.method == 'POST':

#     user_form = UserForm(data = request.POST)

#     profile_form = UserProfileForm(data = request.POST)

#     if user_form.is_valid() and profile_form.is_valid():
#       user = user_form.save()   # save to the database

#       user.set_password(user.password)
#       user.save()

#       profile = profile_form.save(commit = False)
#       profile.user = user

#       if 'picture' in request.FILES:
#         profile_picture = request.FILES['picture']

#       profile.save()

#       is_a_user = True

#     else:
#       print(user_form.errors, profile_form.errors)

#   else:
#     user_form = UserForm()
#     profile_form = UserProfileForm()

#   return render(request, 'app1/register.html', 
#                 {
#                   'user_form': user_form,
#                   'profile_form': profile_form,
#                   'registered': is_a_user
#                 })

  
# def user_login(request):
#   if request.method == 'POST':
#     username = request.POST.get('username')
#     password = request.POST.get('password')


#     user = authenticate(username = username, password = password)

#     if user:
#       if user.is_active:
#         login(request, user)
#         return HttpResponseRedirect(reverse('index'))

#       else:
#         # An inactive account was used - no logging in!
#         return HttpResponse("Your Rango account is disabled.")

#     else:
#       # Bad login details were provided. So we can't log the user in.
#       print("Invalid login details: {0}, {1}".format(username, password))
#       return HttpResponse("Invalid login details supplied.{0} or  {1} is incorrect".format(username, password))

#   else:
#     # No context variables to pass to the template system, hence the
#     # blank dictionary object...
#     return render(request, 'app1/user_login.html', {})



@login_required
def restricted(request):
  return HttpResponse("Since you're logged in, you can see this text!")


# @login_required
# def user_logout(request):
#   logout(request)

#   return HttpResponseRedirect(reverse('index'))
      
      
  
     
